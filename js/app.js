function createLogBox(msg) {
	var logbox = document.createElement("div");
	if (msg.indexOf("[FAIL]") !== -1){
		logbox.className = "logbox error";
	}else if (msg.indexOf("[Server thread/INFO]") !== -1){
		logbox.className = "logbox info";
	}else if (msg.indexOf("[WARN]") !== -1){
		logbox.className = "logbox warning";
	}else if (msg.indexOf("[RCON Listener #1/INFO]") !== -1){
		logbox.className = "logbox msg";
	}else if (msg.indexOf("[User Authenticator #1/INFO]") !== -1){
		logbox.className = "logbox load";
	}else{
		logbox.className = "logbox error";
	}
	
	logbox.innerText=msg;
	return logbox;
}

function loadMoreLogs() {
	isLoading = true;
	let fromPoint = logsTableStartPoint-20;
	let toPoint = logsTableStartPoint;
	if (fromPoint<0){fromPoint=0;}
	if (toPoint<0){toPoint=0;}
	if (toPoint==0 && fromPoint==0){
		isLoading = false;
		console.log('Start Log File, no more');
		return;
	}
	consoleFrame.prepend(loadmsg);
	console.log(fromPoint+' to '+toPoint);
	$.ajax({
		url: 'showLogs',
		data: {from:fromPoint, to: toPoint}, //980 to 1000
		type: 'GET',
		success: function(response) {
			logs = JSON.parse(response);
			consoleFrame.removeChild(loadmsg)
			for (i = logs.length; i > 0; i--) {
				consoleFrame.prepend(createLogBox(logs[i-1]));
			}
			logsTableStartPoint = fromPoint;

			if (consoleFrame.children.length <= 20){
				consoleFrame.scrollTo(0,consoleFrame.scrollHeight); 
			}else {
				consoleFrame.scrollTo(0,600); 
			}
			isLoading = false;
			printLostConnection(false);
		},
		error: function(error) {
			console.log(error);
			printLostConnection(true);
		}
	});
}		
var connectionStatusVar=true;

function printLostConnection(state) {
	if (state!=connectionStatusVar){
		if (state==true) {
			document.getElementById('connectionStatus').innerHTML = 'Utracono Połączenie';
			connectionStatusVar=true;
		}else{
			document.getElementById('connectionStatus').innerHTML = 'Online';
			connectionStatusVar=false;
		}
	}	
}


const updateUptime = function(num) {
	const leadingZero = function(element) {
		if (element < 10) return element = "0" + element;
		return element;
	}
	
	var dt1 = new Date();           
	var dt2 = new Date(parseInt(num+'000'));   
	var ilems1 = dt1.getTime();
	var ilems2 = dt2.getTime();
	var timeDifference = ilems1 - ilems2;

	//console.log(timeDifference+' = '+ilems1+' - '+ilems2);
	
	const msInADay = 24 * 60 * 60 * 1000;
	const eDaysToDate = timeDifference / msInADay;
    const daysToDate = Math.floor(eDaysToDate);

    //musimy tutaj sprawdzic, czy powyzsza zmienna nie jest 0,
    //bo inaczej ponizej bysmy mieli % 0 czyli dzielenie przez 0
    if (daysToDate < 1) {
        daysToDateFix = 1;
    } else {
        daysToDateFix = daysToDate;
    }

    const eHoursToDate = (eDaysToDate % daysToDateFix)*24;
    const hoursToDate = Math.floor(eHoursToDate);

    const eMinutesToDate = (eHoursToDate - hoursToDate)*60;
    const minutesToDate = Math.floor(eMinutesToDate);

    const eSecondsToDate = Math.floor((eMinutesToDate - minutesToDate)*60);
    const secondsToDate = Math.floor(eSecondsToDate);

	const tekst = '  '+hoursToDate+':'+leadingZero(minutesToDate)+'.'+leadingZero(secondsToDate)+'  '+daysToDate+' dni';
	return tekst;		
}




/* ======================================== */
/* LOOPS */
/* ======================================== */
//update uptime number
loopUptimeUpdate=setInterval(function(){
	document.getElementById('uptimevalue').innerHTML = updateUptime(boot_time);
},1000);

//Load more Logs on top consoleFrame
loopLoadMoreLogs=setInterval(function(){
	if (document.getElementById('consoleFrame')) {
		if (!isLoading && consoleFrame.scrollTop == 0 && isSiteFullLoaded) {
			loadMoreLogs();
		}
	}
},100); //0.1 sec
	

var isLoadingNewLogs=false;

//load new logs and update charts
loopLoadNewLogs=setInterval(function(){
	if (document.getElementById('consoleFrame') && (!isLoadingNewLogs)) {
		isLoadingNewLogs=true;
		$.ajax({
			url: 'showNewLogs',
			data: {new:logsTableEndPoint},
			type: 'GET',
			success: function(response) {
				console.log(response);
				var obj = JSON.parse(response);				
				$('.chart.temp').data('easyPieChart').update(obj.charts.temp);
				$('.chart.cpu').data('easyPieChart').update(obj.charts.cpu_usage);
				$('.chart.ram').data('easyPieChart').update(obj.charts.ram_usage);
				
				
				_before = logsTableEndPoint;
				
				for (var id in obj.newLogs) {
					console.log('nowelogi');
					if(obj.newLogs.hasOwnProperty(id)){
						consoleFrame.append(createLogBox(obj.newLogs[id]));
						logsTableEndPoint++;
					}
				}
				if (_before != logsTableEndPoint){
					if (document.getElementById("autoScrollCheckbox").checked){
						//consoleFrame.scrollTo(0,consoleFrame.scrollHeight);
						$("#consoleFrame").animate({ scrollTop: consoleFrame.scrollHeight }, "slow");
					}
				}
				printLostConnection(false);
				isLoadingNewLogs=false;
			},
			error: function(error) {
				isLoadingNewLogs=false;
				printLostConnection(true);
				console.log(error);
			}
		});
	}
},1000); //1 sec


// I don't know but if delete this logs dont print  ¯\_(ツ)_/¯
window.onload = function () { 
	isSiteFullLoaded = true;
	consoleFrame.scrollTo(0,consoleFrame.scrollHeight); 
}
