$(document).ready(function(){

    $("#txtCommand").bind("enterKey",function(e){
      sendRconCommand($("#txtCommand").val());
    });
  
    $("#txtCommand").keyup(function(e){
      if(e.keyCode == 13){
        $(this).trigger("enterKey");
        $(this).val("");
      }
    });
  
    $("#btnSend").click(function(){
      if($("#txtCommand").val() != ""){
        $("#btnSend").prop("disabled", true);
      }
      sendRconCommand($("#txtCommand").val());
    });
      
  });

  function rconState(state, cmd){
    lastcommandoutput = document.getElementById('lastcommandoutput');
    console.log('rconState: '+state); 
    if (state==404){
      lastcommandoutput.innerText ='Command missing';
    }
    else if (state==400){
      lastcommandoutput.innerText ='Unknown command '+cmd; 
    }else if (state=200){
      if (cmd){
        while (lastcommandoutput.firstChild) {
          lastcommandoutput.firstChild.remove();
        }
        $("#lastcommandoutput").append(cmd);
      }
      else{lastcommandoutput.innerText='Without respone';}
    }else if (state==500){
      lastcommandoutput.innerText=cmd;
    }
    else{
     console.log(state); 
    }
    $("#btnSend").prop("disabled", false);
    $("#lastcommandoutput").html($("#lastcommandoutput").text()).text();   
  }
  
  function sendRconCommand(command){
    if (command == "") {
      rconState(404); //Command missing.
      return;
    }
    rconState(0); //waiting
    $.post("rcon/index.php", { cmd: command })
      .done(function(json){
        if(json.status){
          if(json.status == 'success' && json.response && json.command){
            if(json.response.indexOf("Unknown command") != -1){
              rconState(400,json.command+' '+json.response ); //Unknown command
            }
            else if(json.response.indexOf("Usage") != -1){
              rconState(200, json.response);  //succes
            }
            else{
              rconState(200, json.response);
              $("#btnSend").prop("disabled", false);
            }
          }
          else if(json.status == 'error' && json.error){
            rconState(500,json.error); 
          }
          else{
            rconState(500,"Malformed RCON api response"); 
          }
        }
        else{
            rconState(500,"RCON api error (no status returned)"); 
        }
      })
      .fail(function() {
        rconState(500,"RCON error.");
      });
  }
  