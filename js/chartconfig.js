$(function() {
	$('.chart').easyPieChart({
		//your options goes here
		barColor: function(b) {
			return (b < 50 ? "#5cb85c" : b < 85 ? "#f0ad4e" : "#cb3935")
		},
		easing: 'easeOutBounce',
		size: 150,
		scaleLength: 4,
		trackWidth: 8,
		lineWidth: (8 / 1.2),
		lineCap: "square",
		animate: 1000,
		onStep: function(value) {
		  //this.$el.find('.percent').text(~~value+'%');
		  symbol = $(this.el).find('.percent')[0].children[0].innerHTML
		  
		  $(this.el).find('.percent').text(Math.round(value)); //change %
		  $(this.el).find('.percent').append("<i>"+symbol+"</i>");      //add % at the end
		}
	});
});