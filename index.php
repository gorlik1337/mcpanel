<?php
require_once('core.php');
session_start();
?>

<html lang="en"><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="favicon.ico">

    <title>McPanel Login</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="css/signin.css" rel="stylesheet">
  </head>


  <body class="text-center" style='background-image:url("img/server_background.jpg");color: white;'>
    <form class="form-signin" action="scripts/login.php" method="post">
	  <div class="display-3">McPanel</div>
      <h1 class="h3 mb-3 font-weight-normal"><?php echo $tr_header ?></h1>

<?php
if (isset($_SESSION['errormsg'])){
  echo '<div class="alert alert-danger" role="alert">'.$_SESSION['errormsg'].'</div>';
  unset($_SESSION['errormsg']);
}
?>
      <label for="inputLogin" class="sr-only">Login</label>
      <input type="login" name="login" id="inputLogin" class="form-control" placeholder="Login" required="" autofocus="">
      <label for="inputPassword" class="sr-only">Password</label>
      <input type="password" name="pass" id="inputPassword" class="form-control" placeholder="Password" required="">

      <button class="btn btn-lg btn-primary btn-block" type="submit"><?php echo $tr_btn ?></button>
      <p class="mt-5 mb-3">Gorlik © 2018</p>
    </form>
  

</body></html>