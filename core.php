<?php
require_once('config.php');


function nav(){
    echo '
    <nav class="navbar navbar-expand-md navbar-dark bg-dark">
      <a class="navbar-brand" href="$">McPanel</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbar">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Link</a>
          </li>
          <li class="nav-item">
            <a class="nav-link disabled" href="#">Disabled</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="https://example.com" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
            <div class="dropdown-menu" aria-labelledby="dropdown04">
              <a class="dropdown-item" href="#">Action</a>
              <a class="dropdown-item" href="#">Another action</a>
              <a class="dropdown-item" href="#">Something else here</a>
            </div>
          </li>
        </ul>
        <form class="form-inline my-2 my-md-0">
          <a href="../scripts/logout.php" class="btn btn-success my-2 my-sm-0">Logout</a>
        </form>
      </div>
    </nav>
    ';
}

function countLinesOfFile($file){
  $linecount = 0;
  $handle = fopen($file, "r");
  while(!feof($handle)){
  $line = fgets($handle);
  $linecount++;
  }

  fclose($handle);
  return $linecount;
}

function remove_bs($Str) {  
    $StrArr = str_split($Str); $NewStr = '';
    foreach ($StrArr as $Char) {    
      $CharNo = ord($Char);
      if ($CharNo == 163) { $NewStr .= $Char; continue; } // keep £ 
      if ($CharNo > 31 && $CharNo < 127) {
        $NewStr .= $Char;    
      }
    }  
    return $NewStr;
  }



class Uptime {
  private $uptime;

  private $modVals = array(31556926, 86400, 3600, 60, 60);

  public function __construct() {
      $this->read_uptime();
  }

  /**
   * actually trigger a read of the system clock and cache the value
   * @return string
   */
  private function read_uptime() {
      $uptime_raw = @file_get_contents("/proc/uptime");
      $this->uptime = floatval($uptime_raw);
      return $this->uptime;
  }

  private function get_uptime_cached() {
      if(is_null($this->uptime)) $this->read_uptime(); // only read if not yet stored or empty
      return $this->uptime;
  }

  /**
   * recursively run mods on time value up to given depth
   * @param int $d
   * @return int
   **/
  private function doModDep($d) {
      $start = $this->get_uptime_cached();
      for($i=0;$i<$d;$i++) {
          $start = $start % $this->modVals[$i];
      }
      return intval($start / $this->modVals[$d]);
  }

  public function getDays()
  {
      return $this->doModDep(1);
  }

  public function getHours() {
      return $this->doModDep(2);
  }

  public function getMinutes()
  {
      return $this->doModDep(3);
  }

  public function getSeconds()
  {
      return $this->doModDep(4);
  }

  public function uptime()
  {
      return $this->uptime;
  }  

  public function getTime($cached=false) {
      if($cached != false) $this->read_uptime(); // resample cached system clock value
      return sprintf("%03d:%02d:%02d:%02d", $this->getDays(), $this->getHours(), $this->getMinutes(), $this->getSeconds());
  }
}

function boot_time(){
  $abc = new Uptime();
  $pop = $abc -> uptime();

  $date = date("Y-m-d H:i:s");
  $time = strtotime($date);
  $time = $time - ($pop);
  $unix = date("U", $time);
  return $unix;  
}



function serverstats(){
  //cpu stat
  $prevVal = shell_exec("cat /proc/stat");
  $prevArr = explode(' ',trim($prevVal));
  $prevTotal = $prevArr[2] + $prevArr[3] + $prevArr[4] + $prevArr[5];
  $prevIdle = $prevArr[5];
  usleep(0.15 * 1000000);
  $val = shell_exec("cat /proc/stat");
  $arr = explode(' ', trim($val));
  $total = $arr[2] + $arr[3] + $arr[4] + $arr[5];
  $idle = $arr[5];
  $intervalTotal = intval($total - $prevTotal);
  $stat['cpu'] =  intval(100 * (($intervalTotal - ($idle - $prevIdle)) / $intervalTotal));
  $cpu_result = shell_exec("cat /proc/cpuinfo | grep model\ name");
  $stat['cpu_model'] = strstr($cpu_result, "\n", true);
  $stat['cpu_model'] = str_replace("model name    : ", "", $stat['cpu_model']);
  //memory stat
  $stat['mem_percent'] = round(shell_exec("free | grep Mem | awk '{print $3/$2 * 100.0}'"), 2);
  $mem_result = shell_exec("cat /proc/meminfo | grep MemTotal");
  $stat['mem_total'] = round(preg_replace("#[^0-9]+(?:\.[0-9]*)?#", "", $mem_result) / 1024 / 1024, 3);
  $mem_result = shell_exec("cat /proc/meminfo | grep MemFree");
  $stat['mem_free'] = round(preg_replace("#[^0-9]+(?:\.[0-9]*)?#", "", $mem_result) / 1024 / 1024, 3);
  $stat['mem_used'] = $stat['mem_total'] - $stat['mem_free'];
  //hdd stat
  $stat['hdd_free'] = round(disk_free_space("/") / 1024 / 1024 / 1024, 2);
  $stat['hdd_total'] = round(disk_total_space("/") / 1024 / 1024/ 1024, 2);
  $stat['hdd_used'] = $stat['hdd_total'] - $stat['hdd_free'];
  $stat['hdd_percent'] = round(sprintf('%.2f',($stat['hdd_used'] / $stat['hdd_total']) * 100), 2);
  //network stat
  $stat['network_rx'] = round(trim(file_get_contents("/sys/class/net/eth0/statistics/rx_bytes")) / 1024/ 1024/ 1024, 2);
  $stat['network_tx'] = round(trim(file_get_contents("/sys/class/net/eth0/statistics/tx_bytes")) / 1024/ 1024/ 1024, 2);

  //output data by json
  /*echo    
  "{\"cpu\": " . $stat['cpu'] . ", \"cpu_model\": \"" . $stat['cpu_model'] . "\"" . //cpu stats
  ", \"mem_percent\": " . $stat['mem_percent'] . ", \"mem_total\":" . $stat['mem_total'] . ", \"mem_used\":" . $stat['mem_used'] . ", \"mem_free\":" . $stat['mem_free'] . //mem stats
  ", \"hdd_free\":" . $stat['hdd_free'] . ", \"hdd_total\":" . $stat['hdd_total'] . ", \"hdd_used\":" . $stat['hdd_used'] . ", \"hdd_percent\":" . $stat['hdd_percent'] . ", " . //hdd stats
  "\"network_rx\":" . $stat['network_rx'] . ", \"network_tx\":" . $stat['network_tx'] . //network stats
  "}";*/
  return $stat;
}


/* ============== SYSTEM INFO */
/*
// Example
$system = new SystemInfo();
echo "CPU usage: " . $system->getCpuLoadPercentage() . "%\n";
echo "Disc: \n";
print_r($system->getDiskSize(PHP_OS == 'WINNT' ? 'C:' : '/'));
echo "\n\n";
echo "RAM total: " . round($system->getRamTotal() / 1024 / 1024) . " MB \n";
echo "RAM free: " . round($system->getRamFree() / 1024 / 1024) . " MB \n";

*/

/**
 * System infos for Linux (Ubuntu) and Windows.
 *
 * - Total RAM
 * - Free RAM
 * - Disk size
 * - CPU load in %
 */
class SystemInfo
{
    /**
     * Return RAM Total in Bytes.
     *
     * @return int Bytes
     */
    public function getRamTotal()
    {
        $result = 0;
        if (PHP_OS == 'WINNT') {
            $lines = null;
            $matches = null;
            exec('wmic ComputerSystem get TotalPhysicalMemory /Value', $lines);
            if (preg_match('/^TotalPhysicalMemory\=(\d+)$/', $lines[2], $matches)) {
                $result = $matches[1];
            }
        } else {
            $fh = fopen('/proc/meminfo', 'r');
            while ($line = fgets($fh)) {
                $pieces = array();
                if (preg_match('/^MemTotal:\s+(\d+)\skB$/', $line, $pieces)) {
                    $result = $pieces[1];
                    // KB to Bytes
                    $result = $result * 1024;
                    break;
                }
            }
            fclose($fh);
        }
        // KB RAM Total
        return (int) $result;
    }
    /**
     * Return free RAM in Bytes.
     *
     * @return int Bytes
     */
    public function getRamFree()
    {
        $result = 0;
        if (PHP_OS == 'WINNT') {
            $lines = null;
            $matches = null;
            exec('wmic OS get FreePhysicalMemory /Value', $lines);
            if (preg_match('/^FreePhysicalMemory\=(\d+)$/', $lines[2], $matches)) {
                $result = $matches[1] * 1024;
            }
        } else {
            $fh = fopen('/proc/meminfo', 'r');
            while ($line = fgets($fh)) {
                $pieces = array();
                if (preg_match('/^MemFree:\s+(\d+)\skB$/', $line, $pieces)) {
                    // KB to Bytes
                    $result = $pieces[1] * 1024;
                    break;
                }
            }
            fclose($fh);
        }
        // KB RAM Total
        return (int) $result;
    }
    /**
     * Return harddisk infos.
     *
     * @param sring $path Drive or path
     * @return array Disk info
     */
    public function getDiskSize($path = '/')
    {
        $result = array();
        $result['size'] = 0;
        $result['free'] = 0;
        $result['used'] = 0;
        if (PHP_OS == 'WINNT') {
            $lines = null;
            exec('wmic logicaldisk get FreeSpace^,Name^,Size /Value', $lines);
            foreach ($lines as $index => $line) {
                if ($line != "Name=$path") {
                    continue;
                }
                $result['free'] = explode('=', $lines[$index - 1])[1];
                $result['size'] = explode('=', $lines[$index + 1])[1];
                $result['used'] = $result['size'] - $result['free'];
                break;
            }
        } else {
            $lines = null;
            exec(sprintf('df %s', $path), $lines);
            foreach ($lines as $index => $line) {
                if ($index != 1) {
                    continue;
                }
                $values = preg_split('/\s{1,}/', $line);
                $result['size'] = $values[1] * 1024;
                $result['free'] = $values[3] * 1024;
                $result['used'] = $values[2] * 1024;
                break;
            }
        }
        return $result;
    }
    /**
     * Get CPU Load Percentage.
     *
     * @return float load percentage
     */
    public function getCpuLoadPercentage()
    {
        $result = -1;
        $lines = null;
        if (PHP_OS == 'WINNT') {
            $matches = null;
            exec('wmic.exe CPU get loadpercentage /Value', $lines);
            if (preg_match('/^LoadPercentage\=(\d+)$/', $lines[2], $matches)) {
                $result = $matches[1];
            }
        } else {
            // https://github.com/Leo-G/DevopsWiki/wiki/How-Linux-CPU-Usage-Time-and-Percentage-is-calculated
            //$tests = array();
            //$tests[] = 'cpu  3194489 5224 881924 305421192 603380 76 52143 106209 0 0';
            //$tests[] = 'cpu  3194490 5224 881925 305422568 603380 76 52143 106209 0 0';
            $checks = array();
            foreach (array(0, 1) as $i) {
                $cmd = '/proc/stat';
                #$cmd = 'grep \'cpu \' /proc/stat <(sleep 1 && grep \'cpu \' /proc/stat) | awk -v RS="" \'{print ($13-$2+$15-$4)*100/($13-$2+$15-$4+$16-$5) "%"}\'';
                #exec($cmd, $lines);
                $lines = array();
                $fh = fopen($cmd, 'r');
                while ($line = fgets($fh)) {
                    $lines[] = $line;
                }
                fclose($fh);
                //$lines = array($tests[$i]);
                foreach ($lines as $line) {
                    $ma = array();
                    if (!preg_match('/^cpu  (\d+) (\d+) (\d+) (\d+) (\d+) (\d+) (\d+) (\d+) (\d+) (\d+)$/', $line, $ma)) {
                        continue;
                    }
                    /**
                     * The meanings of the columns are as follows, from left to right:
                      1st column : user = normal processes executing in user mode
                      2nd column : nice = niced processes executing in user mode
                      3rd column : system = processes executing in kernel mode
                      4th column : idle = twiddling thumbs
                      5th column : iowait = waiting for I/O to complete
                      6th column : irq = servicing interrupts
                      7th column : softirq = servicing softirqs
                      8th column:
                      9th column:
                      Calculation:
                      sum up all the columns in the 1st line "cpu" :
                      ( user + nice + system + idle + iowait + irq + softirq )
                      this will yield 100% of CPU time
                      calculate the average percentage of total 'idle' out of 100% of CPU time :
                      ( user + nice + system + idle + iowait + irq + softirq ) = 100%
                      ( idle ) = X %
                      TOTAL USER = %user + %nice
                      TOTAL CPU = %user + %nice + %system
                      TOTAL IDLE = %iowait + %steal + %idle
                     */
                    $total = $ma[1] + $ma[2] + $ma[3] + $ma[4] + $ma[5] + $ma[6] + $ma[7] + $ma[8] + $ma[9];
                    //$totalCpu = $ma[1] + $ma[2] + $ma[3];
                    //$result = (100 / $total) * $totalCpu;
                    $ma['total'] = $total;
                    $checks[] = $ma;
                    break;
                }
                if ($i == 0) {
                    // Wait before checking again.
                    sleep(1);
                }
            }
            // Idle - prev idle
            $diffIdle = $checks[1][4] - $checks[0][4];
            // Total - prev total
            $diffTotal = $checks[1]['total'] - $checks[0]['total'];
            // Usage in %
            $diffUsage = (1000 * ($diffTotal - $diffIdle) / $diffTotal + 5) / 10;
            $result = $diffUsage;
        }
        return (float) $result;
    }
}

?>