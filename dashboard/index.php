<?php
require_once('../core.php');
session_start();

if (!isset($_SESSION['loggedin']) or $_SESSION['loggedin'] != true){header("Location: ../index.php");die();}

$stat = serverstats();
?>


<html lang="en"><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="../favicon.ico">

    <title>McPanel Dashboard</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="../css/dashboard.css" rel="stylesheet">
    <script>
        //load variables
		var logsTableStartPoint = <?php echo countLinesOfFile($config_pathtologs)-1 ?>;
		var logsTableEndPoint = <?php echo countLinesOfFile($config_pathtologs)-1 ?>;
		var boot_time = <?php echo boot_time(); ?>;
		var isLoading = false;
		var isSiteFullLoaded = false;
		loadmsg = document.createElement("div");
		loadmsg.className = "logbox loading";
		loadmsg.innerText="Ładowanie...";
	</script>

  </head>

  <body>
    <?php nav(); ?>


    <div class="container-fluid sitecontent"> 
        <br>
		<h1 class="display-4">Dashboard <?php echo $discusageproc; ?> </h1>
		<h5><?php echo $config_ip ?></h5>
		
        <div class="row">
			<div class="col-md-8">
				<h4><?php echo $tr_consoleheader ?></h4>			
				<div id="consoleFrame">
					
				</div>
				<div class="input-group mb-3">
					<input type="text" class="form-control" placeholder=">" aria-label=">" aria-describedby="txtCommand" id="txtCommand">
					<div class="input-group-prepend">
						<div class="input-group-text">
							<input type="checkbox" id="autoScrollCheckbox" aria-label="AutoScroll" checked>
						</div>
					</div>
					<div class="input-group-append">
						<button class="btn btn-outline-success" type="button" id="btnSend">Wykonaj</button>
					</div>
				</div>

				<div class="input-group mb-3">
					<div class="input-group-prepend" style="width: 100%">
						<div style="width: 100%" id="lastcommandoutput">Last RCON command output</div>
					</div>
				</div>

			</div>
			<div class="col-md-4">
				<h4><?php echo $tr_timersheader ?></h4>
				<div class="row">
					<span class="chart temp" data-percent="0">
						<span class="percent">0<i>°C</i></span>
						<span class="label">Temp</span>
					</span>
					<span class="chart cpu" data-percent="0">
						<span class="percent">0<i>%</i></span>
						<span class="label">CPU Usage</span>
					</span>
					<span class="chart disk" data-percent="<?php echo $stat['hdd_percent']; ?>">
						<span class="percent"><?php echo $stat['hdd_percent']; ?><i>%</i></span>
						<span class="label">Local Disc Space</span>
					</span>
					<span class="chart ram" data-percent="0">
						<span class="percent">0<i>%</i></span>
						<span class="label">RAM</span>
					</span>
				</div>
				
				<h4><?php echo $tr_anotherparametrsheader ?></h4>
				<div class="row">
					<div class="infobox">
						<span class="name">Status połączenia</span>
						<span class="value" id="connectionStatus"><?php echo $tr_waiting; ?></span>
					</div>
					
					<div class="infobox">
						<span class="name">Uptime</span>
						<span class="value" id="uptimevalue"><?php echo $tr_waiting; ?></span>
					</div>

					<div class="infobox">
						<span class="name"><?php echo $tr_cpumodel; ?></span>
						<span class="value" id="uptimevalue"><?php echo $stat['cpu_model']?></span>
					</div>

					<div class="infobox">
						<span class="name">RAM</span>
						<span class="value" id="uptimevalue"><?php echo $stat['mem_used'].'/'.$stat['mem_total']?> GB</span>
					</div>

					<div class="infobox">
						<span class="name">Disc</span>
						<span class="value" id="uptimevalue"><?php echo $stat['hdd_used'].'/'.$stat['hdd_total']?> GB</span>
					</div>
			
				</div>
			</div>
        </div>
 
        <footer class="footer">
            <p> Gorlik &copy; 2018 </p>
        </footer>
    </div>
	<script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script type="text/javascript" src="../js/jquery.easy-pie-chart.js"></script>
	<link href="../js/jquery.easy-pie-chart.css" rel="stylesheet">
	<script type="text/javascript" src="../js/chartconfig.js"></script>
	
	<script type="text/javascript" src="../js/app.js"></script>
	<script type="text/javascript" src="../js/rcon.js"></script>

</body></html>