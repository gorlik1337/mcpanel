<?php

$from = intval($_GET['from']);
if (!is_int($from) or !isset($_GET['from'])){die('from must be int!');}

$to = intval($_GET['to']);
if (!is_int($to) or !isset($_GET['to'])){die('to must be int!');}

require_once('../../core.php');
session_start();
if (!isset($_SESSION['loggedin']) or $_SESSION['loggedin'] != true){die('no session');}



$handle = fopen($config_pathtologs, "r");
if ($handle) {
    $logsTab = array();
    $i=0;
    while (($line = fgets($handle)) !== false) {
        //$line[strlen($line)-2] = NULL;
        $line = substr($line,0,strlen($line)-1);

        array_push($logsTab, htmlspecialchars($line));
        $i++;
    }
    fclose($handle);
} else {
    die('Error Open Log file');
} 



for ($i=0; $from+$i < $to; $i++){
    $out[$i] = htmlspecialchars($logsTab[$from+$i]);
}
echo json_encode($out);
?>