<?php
$Tstart = microtime(true); 

$new = intval($_GET['new']);
if (!is_int($new) or !isset($_GET['new'])){die('new must be int!');}


require_once('../../core.php');
session_start();
if (!isset($_SESSION['loggedin']) or $_SESSION['loggedin'] != true){die('no session');}


$handle = fopen($config_pathtologs, "r");
if ($handle) {
    $logsTab = array();
    $i=0;
    while (($line = fgets($handle)) !== false) {
        //$line[strlen($line)-2] = NULL;
        $line = substr($line,0,strlen($line)-1);

        array_push($logsTab, htmlspecialchars($line));
        $i++;
    }
    fclose($handle);
} else {
    die('Error Open Log file');
} 

$newLogs = array();

$newstart = $new;
while (true){
    if (isset($logsTab[$new])){
        $newLogs[$new] = $logsTab[$new];
        $new++;
    }else{
        break;
    }
}

$system = new SystemInfo();
$ramtotal = round($system->getRamTotal() / 1024 / 1024);
$ramfree = round($system->getRamFree() / 1024 / 1024);
$ramuse = $ramtotal - $ramfree;
$ramuseproc = $ramuse / $ramtotal * 100;

$Tstop = microtime(true); 


$output='{"charts": {"ms": "'.round((($Tstop - $Tstart) * 1000), 0).'", "cpu_usage": '.round($system->getCpuLoadPercentage()).',"temp": 99,"ram_usage": '.round($ramuseproc).'},"newLogs": {';

for ($i = 0; $i < count($newLogs); $i++)
{
    $logmessage = utf8_encode(utf8_decode(htmlspecialchars($newLogs[$newstart], ENT_QUOTES, "UTF-8")));
    $logmessage = remove_bs($logmessage);

    $output=$output.' "'.$newstart.'": "'.$logmessage .'"';
    if ($i+1 < count($newLogs)){$output=$output.',';}
    $newstart++;
}
$output=$output.'}}';

//output headers
//header('Content-type: text/json');
//header('Content-type: application/json');

echo $output;
die();

?>