<?php
require_once('../core.php');
session_start();

function errormsg($errormessage){
    $_SESSION['errormsg'] = $errormessage;
    header("Location: ../index.php");
    die();
}

if (!isset($_POST['login'])){errormsg($tr_blanklogin);}
if (!isset($_POST['pass'])){errormsg($tr_blankpass);}

if($_POST['login'] == $config_login){
    if($_POST['pass'] == $config_pass){
        $_SESSION['loggedin'] = true;
        header("Location: ../dashboard");
        die();
    }else{errormsg($tr_errorlogin);}
}else{errormsg($tr_errorlogin);}

?>